github-to-jira
==============

Github Issues to Jira converter script

To use, first install pygithub3 packages with:

```
easy_install -U pygithub3
```

Edit github_export.py script, with your account, projects and mappings.
Execute github_export.py, issue data will be in output.csv

Import in Jira in Administration -> System -> Import & Export -> External System Import -> Import from CSV
Select output.csv in "CSV Source File" and check box "Use an existing configuration file". Select CSV-configuration.txt.
Next, Next, And select your mappings. Done!

Commits will appear as comments.

Enjoy!
