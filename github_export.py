# -*- coding: UTF-8 -*- 

from datetime import datetime
from pygithub3 import Github
from pygithub3.exceptions import NotFound
import csv
import re

github_user = "github_username"
github_pass = "***"
github_project = "project_to_export"
github_org = "org_if_you_have"
jira_project = "JIRAPROJECTPREFIX"

#first in github, after in jira
user_map = {
	"github_user":"jira_user",
}

prio_map = {
	"github_label":"jira_priority",
}

type_map = {
	"github_label":"jira_type",
}

status_map = {
	"github_label":"jira_status",
}

def get_map(dict_map, labels):
	for i in labels:
		try:
			return dict_map[i.name]
		except KeyError, e:
			continue
			
def format_text(text):
	#Transform references to jira issues
	text = re.subn(r'#([0-9]+)',r'%s-\1'%jira_project,text)[0]
	text = re.subn(r"<!--.*-->",'',text,flags=re.S)[0]
	text = text.replace('"',"'")
	
	return text

auth = dict(login=github_user, password=github_pass)
if github_org:
	gh = Github(user=github_org,repo=github_project,**auth)
	all_repos = set([i.name for i in gh.repos.list_by_org(github_org, type='all').all()])
elif github_user:
	gh = Github(user=github_user,repo=github_project,**auth)
	all_repos = set([i.name for i in gh.repos.list(github_user, type='all').all()])

fieldnames = [
	"updated", "status", "created", "summary", "issuetype", "description",
	"issueid", "milestone", "reporter", "assigned","priority","closed", "resolution", 
]
fieldnames += ["comment"] * 30


issues = gh.issues.list_by_repo()
issues_closed = gh.issues.list_by_repo(state='closed')

issues = issues.all()
issues += issues_closed.all()


out = csv.writer(file("output.csv","wb"))
out.writerow(fieldnames)
for i in issues:
	issue = [
		i.updated_at,
		get_map(status_map, i.labels),
		i.created_at,
		i.title.encode('utf-8'),
		get_map(type_map, i.labels),
		format_text(i.body.encode('utf-8')),
		jira_project + "-" + str(i.number),
		i.milestone.title,
		user_map.get(i.user.login),
		user_map.get(i.assignee.login),
		get_map(prio_map, i.labels),
		i.closed_at,
	]
    
	#Resolution
	if i.state == "closed" or issue[1] == "Closed":
		#Mark as closed based on github state
		issue[1] = "Closed"
		issue.append("Fixed")
	else:
		issue.append("")

	print "Issue #%s, comentarios %s." % (i.number, i.comments)
	for c in gh.issues.comments.list(i.number).all():
		print "Appending comment: %s" % c
		body = format_text(c.body)
		body = body.encode('utf-8')
		comment = """%(created_at)s; %(user)s; %(body)s""" % {"created_at": c.created_at, "user": user_map.get(c.user.login), "body": body}
		issue.append(comment)

	for c in gh.issues.events.list_by_issue(i.number).all():
		comment = None
		#Commit references
		if c.event == "referenced":
			commit = None
			for r in all_repos:
				try:
					commit = gh.repos.commits.get(c.commit_id, repo=r)
					break
				except NotFound:
					pass

			if commit:	
				body = "Referenced in %(url)s" % {"url": commit.html_url}
				comment = """%(created_at)s; %(user)s; %(body)s""" % {"created_at": c.created_at, "user": user_map.get(c.actor.login), "body": body}
		
		if comment:
			print "Appending event: %s" % c
			issue.append(comment)

	[issue.append("") for r in range(30 - i.comments)]
	out.writerow(issue)

